export const GET_VEHICLE_YEARS = 'GET_VEHICLE_YEARS';
export const HAS_ERROR = 'HAS_ERROR';
export const GET_VEHICLE_MAKES = 'GET_VEHICLE_MAKES';
export const GET_VEHICLE_MODELS = 'GET_VEHICLE_MODELS';
export const GET_VEHICLE_OIL_TYPES = 'GET_VEHICLE_OIL_TYPES';
export const GET_VEHICLE_FILTER_TYPES = 'GET_VEHICLE_FILTER_TYPES';
export const GET_AVAILABILITY = 'GET_AVAILABILITY';
export const GET_STATE_LIST = 'GET_STATE_LIST';
export const GET_VEHICLE_BOOKINGS = 'GET_VEHICLE_BOOKINGS';
export const GET_TOGGLE_POPUP_STATUS = 'GET_TOGGLE_POPUP_STATUS';
export const GET_CARQUERY_YEARS = 'GET_CARQUERY_YEARS';
export const GET_CARQUERY_MAKES = 'GET_CARQUERY_MAKES';
export const GET_CARQUERY_MODELS = 'GET_CARQUERY_MODELS';
export const GET_CARQUERY_TRIMS = 'GET_CARQUERY_TRIMS';



